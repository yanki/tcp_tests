%%% @author Yana P. Ribalchenko <yan4ik@gmail.com>
%%% @copyright (C) 2015, Yana P. Ribalchenko
%%% @doc
%%% my tcp_server for connection of telnet
%%% local server
%%% @end
%%% Created :  7 Oct 2015 by Yana P. Ribalchenko <yan4ik@gmail.com>
%%%-------------------------------------------------------------------
-module(tcp_serv_RFC868).

%% API
-export ([server/0]).

%% TimeOut for connection,in milliseconds. 
-define (TIMEOUT, 5000).

%% Port for connection
-define (PORT, 3737).

%%%===================================================================
%%% API
%%%===================================================================
%%--------------------------------------------------------------------
%% @doc start tcp_server
%% @end
%%--------------------------------------------------------------------
-spec server() -> ok.
server () ->
    {ok, LSock} = gen_tcp:listen(
                    ?PORT, 
                    [binary, {packet, 0}, {reuseaddr, true}, {active, false}]),
    wait_conn (LSock),
    gen_tcp:close(LSock).

%%%===================================================================
%%% Internal functions
%%%===================================================================
wait_conn(LSock) ->
    {ok, Sock} = gen_tcp:accept(LSock),
    Seconds =
        calendar:datetime_to_gregorian_seconds(
          %% здесь дата на -2 часа, но пока и так не работает -UT
          calendar:now_to_universal_time( now()))-719528*24*3600,
    Date1=calendar:gregorian_seconds_to_datetime(Seconds),
    
    Sec =
        if Seconds > 4294967295 ->
                io:format("integer overflow!~n", []),
                2629584000;
           true ->
                Seconds
        end,
    SecBin = 
           binary:encode_unsigned (Sec),
    io:format ("Date:~p~n", [Date1]),
    io:format ("SecBin:~p~n", [SecBin]),
    io:format ("In: ~.16b~n", [Sec]),
    %%ok = gen_tcp:send(Sock, <<SecBin:32/integer>>),
    ok = gen_tcp:send(Sock, SecBin),
    gen_tcp:close(Sock).
