%%% @author Yana P. Ribalchenko <yan4ik@gmail.com>
%%% @copyright (C) 2015, Yana P. Ribalchenko
%%% @doc
%%% my tcp_server for connection of telnet
%%%  Character Generator Protocol RFC 864
%%% local server
%%% @end
%%% Created :  7 Oct 2015 by Yana P. Ribalchenko <yan4ik@gmail.com>
%%%-------------------------------------------------------------------
-module(tcp_serv_RFC864_b).

%% API
-export ([server/0,
          go_recv/1,
          go_send/2]).

%% TimeOut for connection,in milliseconds. 
-define (TIMEOUT, 5000).

%% Port for connection
-define (PORT, 1919).

%%%===================================================================
%%% API
%%%===================================================================
%%--------------------------------------------------------------------
%% @doc start tcp_server
%% @end
%%--------------------------------------------------------------------
-spec server() -> ok.
server () ->
    {ok, LSock} = gen_tcp:listen(
                    ?PORT, 
                    [binary, {packet, 0}, {reuseaddr, true}, {active, false}]),
    wait_conn (LSock),
    gen_tcp:close(LSock).

%%%===================================================================
%%% Internal functions
%%%===================================================================
wait_conn(LSock) ->
    {ok, Sock} = gen_tcp:accept(LSock),
%%    spawn (tcp_serv_RFC864, go_send, [Sock, 33] ),
    spawn (tcp_serv_RFC864, go_send, [Sock, 0] ),
    spawn (tcp_serv_RFC864, go_recv, [Sock] ),
    wait_conn(LSock).

%% go_send(Sock, X) ->
%%     if 
%%         X < 56 -> gen_tcp:send(Sock, lists:seq(X, X+71));
%%         true -> go_send(Sock, 33)
%%     end,
%%     go_send(Sock, X+1).
%%CR LF 13 10

go_send(Sock, Packet) ->
    Packet=lists:reverse (form_list(33, [])),
    io:format ("Packet=~p~n",[Packet]),
    gen_tcp:send(Sock, Packet),
%% следующий пакет другой должен быть!
    go_send(Sock, Packet).

form_list(_X, InList) when length(InList) == 70 ->
    io:format ("List=~p~n",[InList]),
    [10|[13|InList]];
form_list(X, InList) when  X > 33 andalso X < 127 -> 
    io:format ("X=~p, Listing=~p~n",[X, InList]),
            form_list(X+1, [X|InList]);
form_list(X, InList) when X >= 127-> 
    io:format ("X=~p, ListLastX=~p~n",[X, InList]),
    form_list(33, InList).




    %% if
    %%     X > 33 andalso X < 127 -> 
    %% io:format ("X=~p, Listing=~p~n",[X, InList]),
    %%         form_list(X+1, [X|InList]);
    %%     true-> form_list(33, InList)
    %% end.

    
go_recv (Sock) ->
    case gen_tcp:recv(Sock, 0) of
        {ok, _M} ->    
%% io:format("Sock=~p,~n Msg=~p~n", [Sock, M]),
            ok;
        {error,ebadf} -> 
            io:format("stop~n", []),            
            gen_tcp:close(Sock);
       _  ->             
%%            io:format("Sock=~p~n, =~p~n", [Sock, Bb]),
            neok
    end,
%%    io:format("start5~nSock=~p~n", [Sock]),
    go_recv(Sock).


