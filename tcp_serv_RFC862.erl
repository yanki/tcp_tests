%%% @author Yana P. Ribalchenko <yan4ik@gmail.com>
%%% @copyright (C) 2015, Yana P. Ribalchenko
%%% @doc
%%% my tcp_server for connection of telnet
%%% Echo Protocol RFC 862
%%% local server
%%% @end
%%% Created :  7 Oct 2015 by Yana P. Ribalchenko <yan4ik@gmail.com>
%%%-------------------------------------------------------------------
-module(tcp_serv_RFC862).

%% API
-export ([server/0, 
          go_recv/1]).

%% TimeOut for connection,in milliseconds. 
-define (TIMEOUT, 5000).

%% Port for connection
-define (PORT, 7777).

%%%===================================================================
%%% API
%%%===================================================================
%%--------------------------------------------------------------------
%% @doc start tcp_server
%% @end
%%--------------------------------------------------------------------
-spec server() -> ok.
server () ->
    {ok, LSock} = gen_tcp:listen(
                    ?PORT, 
                    [binary, {packet, 0}, {reuseaddr, true}, {active, false}]),
    wait_conn (LSock),
    gen_tcp:close(LSock).

%%%===================================================================
%%% Internal functions
%%%===================================================================
wait_conn(LSock) ->
    {ok, Sock} = gen_tcp:accept(LSock),
    spawn (tcp_serv_RFC862, go_recv, [Sock] ),
    %%            io:format("Pid=~p~n", [Pid]),
    wait_conn(LSock).


go_recv (Sock) ->
    %%            io:format("SockOpen=~p~n", [Sock]),
    case gen_tcp:recv(Sock, 0) of
        {ok, M} ->    
            %%io:format("Sock=~p,~n self()=~p~n Msg=~p~n", [Sock, self(), M]),
            gen_tcp:send (Sock, M),
            go_recv(Sock); 
        _ -> 
            %%            io:format("SockClose=~p, Bb=~p~n", [Sock, Bb]),
            ok = gen_tcp:close(Sock)
    end.
