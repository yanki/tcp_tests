%%% @author Yana P. Ribalchenko <yan4ik@gmail.com>
%%% @copyright (C) 2015, Yana P. Ribalchenko
%%% @doc
%%% my tcp_server for connection of telnet
%%% Discard Protocol RFC 863
%%% local server
%%% @end
%%% Created :  7 Oct 2015 by Yana P. Ribalchenko <yan4ik@gmail.com>
%%%-------------------------------------------------------------------
-module(tcp_serv_RFC863).

%% API
-export ([server/0,
          go_recv/1]).

%% TimeOut for connection,in milliseconds. 
-define (TIMEOUT, 5000).

%% Port for connection
-define (PORT, 9999).

%%%===================================================================
%%% API
%%%===================================================================
%%--------------------------------------------------------------------
%% @doc start tcp_server
%% @end
%%--------------------------------------------------------------------
-spec server() -> ok.
server () ->
    {ok, LSock} = gen_tcp:listen(
                    ?PORT, 
                    [binary, {packet, 0}, {reuseaddr, true}, {active, false}]),
    wait_conn (LSock),
    gen_tcp:close(LSock).

%%%===================================================================
%%% Internal functions
%%%===================================================================
wait_conn(LSock) ->
    {ok, Sock} = gen_tcp:accept(LSock),
    spawn (tcp_serv_RFC863, go_recv, [Sock] ),
    wait_conn(LSock).

go_recv (Sock) ->
    case gen_tcp:recv(Sock, 0) of
        {ok, _M} ->    
%% io:format("Sock=~p,~n Msg=~p~n", [Sock, M]),
            ok;
        {error,ebadf} -> 
            io:format("stop~n", []),            
            gen_tcp:close(Sock);
        _  ->             
%%            io:format("Sock=~p~n, =~p~n", [Sock, Bb]),
            neok
    end,
%%    io:format("start5~nSock=~p~n", [Sock]),
    go_recv(Sock).


