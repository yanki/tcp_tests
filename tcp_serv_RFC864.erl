%%% @author Yana P. Ribalchenko <yan4ik@gmail.com>
%%% @copyright (C) 2015, Yana P. Ribalchenko
%%% @doc
%%% my tcp_server for connection of telnet
%%%  Character Generator Protocol RFC 864
%%% local server
%%% @end
%%% Created :  7 Oct 2015 by Yana P. Ribalchenko <yan4ik@gmail.com>
%%%-------------------------------------------------------------------
-module(tcp_serv_RFC864).

%% API
-export ([server/0,
          go_recv/1,
          go_send/4]).

%% TimeOut for connection,in milliseconds. 
-define (TIMEOUT, 5000).

%% Port for connection
-define (PORT, 1919).

%%%===================================================================
%%% API
%%%===================================================================
%%--------------------------------------------------------------------
%% @doc start tcp_server
%% @end
%%--------------------------------------------------------------------
-spec server() -> ok.
server () ->
    {ok, LSock} = gen_tcp:listen(
                    ?PORT, 
                    [binary, {packet, 0}, {reuseaddr, true}, {active, false}]),
    wait_conn (LSock),
    gen_tcp:close(LSock).

%%%===================================================================
%%% Internal functions
%%%===================================================================
wait_conn(LSock) ->
    io:format("Waiting for new connection...~n", []),
    {ok, Sock} = gen_tcp:accept(LSock),
    io:format("New connection~n", []),
    _Pid = spawn (tcp_serv_RFC864, go_send, [Sock, 33, 33, []] ),
    spawn (tcp_serv_RFC864, go_recv, [Sock] ),
    wait_conn(LSock).

go_send(Sock, X1, X, List) ->
    Packet = 
        if X1 >= 127 andalso X >= 127 ->
                form_list(33, 33, []);
           true -> form_list(X1, X, List)
        end,
    case gen_tcp:send(Sock, Packet) of
        ok -> 
            if
                X1 == 126 ->
                    go_send(Sock, 33, 33, []);
                true ->
                    go_send (Sock, X1+1, X1+1, [])
            end;
        _ ->
            io:format("go_send [~p] stopped.~n", [Sock]),
            ignore
    end.


form_list(_X1, _X, InList) when length(InList) == 72 ->
    lists:reverse ([10|[13|InList]]);
form_list(X1, X, InList) when  X >= 33 andalso X < 127 -> 
    form_list(X1, X+1, [X|InList]);
form_list(X1, X, InList) when X == 127-> 
    form_list(X1, 33, InList);
form_list(X1, _X, _InList) when X1 >= 127 -> 
    form_list(33, 33, []).

go_recv (Sock) ->
    case gen_tcp:recv(Sock, 0) of
        {ok, M} ->    
            io:format("Sock=~p,~n self()=~p~n Msg=~p~n", [Sock, self(), M]),
            go_recv (Sock);
        _ -> 
            io:format("go_recv [~p] stopped.~n", [Sock]),
            stop
    end.

