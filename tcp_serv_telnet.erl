%%% @author Yana P. Ribalchenko <yan4ik@gmail.com>
%%% @copyright (C) 2015, Yana P. Ribalchenko
%%% @doc
%%% my tcp_server for connection of telnet
%%% local server
%%% @end
%%% Created :  7 Oct 2015 by Yana P. Ribalchenko <yan4ik@gmail.com>
%%%-------------------------------------------------------------------
-module(tcp_serv_telnet).

%% API
-export ([server/0]).

%% TimeOut for connection,in milliseconds. 
-define (TIMEOUT, 5000).

%% Port for connection
-define (PORT, 3334).

%%%===================================================================
%%% API
%%%===================================================================
%%--------------------------------------------------------------------
%% @doc start tcp_server
%% @end
%%--------------------------------------------------------------------
-spec server() -> ok.
server () ->
    {ok, LSock} = gen_tcp:listen(
                    ?PORT, 
                    [binary, {packet, 0}, {reuseaddr, true}, {active, false}]),
    wait_conn (LSock).

%%%===================================================================
%%% Internal functions
%%%===================================================================
wait_conn(LSock) ->
    {ok, Sock} = gen_tcp:accept(LSock),
    case go_recv(Sock, <<>>) of
        stop ->
            stop;
        _ ->
            wait_conn(LSock)
    end.

go_recv (Sock, B) -> 
    case gen_tcp:recv(Sock, 0, ?TIMEOUT) of
        {ok,<<"stop\r\n">>} ->
            gen_tcp:close(Sock),
            stop;
        {ok, Brr} ->
            io:format("~p~n", [Brr]),
             go_recv(Sock, Brr);
        T ->
            T,
             go_recv(Sock, B)
    end.
