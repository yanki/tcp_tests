%%% @author Yana P. Ribalchenko <yan4ik@gmail.com>
%%% @copyright (C) 2015, Yana P. Ribalchenko
%%% @doc
%%% my tcp_server for connection of telnet
%%% local server
%%% @end
%%% Created :  7 Oct 2015 by Yana P. Ribalchenko <yan4ik@gmail.com>
%%%-------------------------------------------------------------------
-module(tcp_serv_2).

%% API
-export ([server/0]).

%% TimeOut for connection,in milliseconds. 
-define (TIMEOUT, 5000).

%% Port for connection
-define (PORT, 3737).

%%%===================================================================
%%% API
%%%===================================================================
%%--------------------------------------------------------------------
%% @doc start tcp_server
%% @end
%%--------------------------------------------------------------------
-spec server() -> ok.
server () ->
    {ok, LSock} = gen_tcp:listen(
                    ?PORT, 
                    [binary, {packet, 0}, {reuseaddr, true}, {active, false}]),
    wait_conn (LSock),
    gen_tcp:close(LSock).

%%%===================================================================
%%% Internal functions
%%%===================================================================
wait_conn(LSock) ->
    {ok, Sock} = gen_tcp:accept(LSock),
%%%    DateTime=calendar:datetime1970(calendar:local_time()),
%%    DateTime =  calendar:local_time(),
%%    ok = gen_tcp:close(Sock),
    %% DT = calendar:now_to_universal_time(now()),
    %% io:format ("DT:~p~n", [DT]),
    %% Sec = calendar:datetime_to_gregorian_seconds( DT)-719528*24*3600,
    %% Sec =
    %%     if Seconds > 4294967295 ->
    %%             io:format("integer overflow!~n", []),
    %%             2629584000;
    %%        true ->
    %%             Seconds
    %%     end,
%%%%%%%%%%%%2208988800),
    %% ok = gen_tcp:send(Sock, [<<2629584000:32/integer>>]),


%% дата от 1970 года    
    Seconds =
        calendar:datetime_to_gregorian_seconds(
          calendar:now_to_local_time( now()))-719528*24*3600,
%% отображаем дату для себя
    Date1=calendar:gregorian_seconds_to_datetime(Seconds+719528*24*3600),
    
    Sec =
        if Seconds > 4294967295 ->
                io:format("integer overflow!~n", []),
                2629584000;
           true ->
                Seconds
        end,
    
    SecBin = 
    %%     list_to_binary(integer_to_list(Sec)),
           binary:encode_unsigned (Sec),
%%     integer_to_binary(Integer, Base) -> binary()
    io:format ("Date:~p~n", [Date1]),
    io:format ("SecBin:~p~n", [SecBin]),
    io:format ("In: ~.16b~n", [Sec]),
    %%ok = gen_tcp:send(Sock, <<SecBin:32/integer>>),
    ok = gen_tcp:send(Sock, SecBin),
    gen_tcp:close(Sock).

      







  

%% server_udp(Port) ->
%%     {ok, Socket} = gen_udp:open(Port, [binary]),
%%     loop(Socket).
%% loop(Socket) ->
%%     receive
%%         {udp, Socket, Host, Port, Bin} ->
%%             BinReply = ... ,
%%             gen_udp:send(Socket, Host, Port, BinReply),
%%             loop(Socket)
%%     end.

%%%hole% rdate -p -o 3737 localhost
