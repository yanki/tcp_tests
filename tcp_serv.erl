%%% @author Yana P. Ribalchenko <yan4ik@gmail.com>
%%% @copyright (C) 2015, Yana P. Ribalchenko
%%% @doc
%%% my tcp_server from example
%%% local server
%%% @end
%%% Created :  7 Oct 2015 by Yana P. Ribalchenko <yan4ik@gmail.com>

-module(tcp_serv).
-export ([server/0, client/0]).

server () ->
    {ok, LSock} = gen_tcp:listen(3334, [binary, {packet, 0}, {active, false}]),
    {ok, Sock} = gen_tcp:accept(LSock),
    {ok, Bin} = go_recv(Sock, []),
    ok = gen_tcp:close(Sock),
    Bin.

client () ->
    SomeHost = "localhost",
    {ok, Sock} = gen_tcp:connect(SomeHost, 3334, [binary, {packet, 0}]),
%%    Time = calendar:local_time() ,
    ok = gen_tcp:send (Sock, "Time"),
    ok = gen_tcp:close (Sock).

go_recv (Sock, List) -> 
    case gen_tcp:recv(Sock, 0) of
        {ok, B} -> go_recv(Sock, [List, B]);
        {error, closed} ->
            {ok, list_to_binary(List)}
    end.
   
    
    
